class Funcionario:
    def __init__(self, codigo, nome, cargo):
        self.codigo = codigo
        self.nome = nome
        self.cargo = cargo


def add_cargo(salario):
    vetor_cargos.append(salario)


def add_funcionario(codigo, nome, cargo):
    for funcionario in vetor_funcionarios:
        if funcionario.codigo == codigo:
            raise Exception("Código de funcionário já existente!")

    if(cargo < 0 or cargo > len(vetor_cargos)):
        raise Exception("Cargo inválido!")

    else:
        vetor_funcionarios.append(Funcionario(codigo, nome, cargo))


def relatorio():
    for funcionario in vetor_funcionarios:
        print("Código: {}\n Nome: {}\n Salário: {}\n----".format(
            funcionario.codigo, funcionario.nome, vetor_cargos[funcionario.cargo]))


def total_salarios(cargo):
    if cargo > len(vetor_cargos) or cargo<0:
        raise Exception("cargo inexistente!")
    total = 0
    for funcionario in vetor_funcionarios:
        if funcionario.cargo == cargo:
            total += vetor_cargos[cargo]
    return total
